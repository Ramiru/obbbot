const Sequelize = require('sequelize');

const db = new Sequelize(process.env.DATABASE_URL);

const Chat = db.define('chat', {
    chat_id: Sequelize.INTEGER,
    last_game_id: Sequelize.INTEGER
})

const User = db.define('user', {
    chat_id: Sequelize.INTEGER,
    user_id: Sequelize.INTEGER,
    username: Sequelize.STRING,
    is_active: Sequelize.BOOLEAN,
    count: Sequelize.INTEGER,
})

const Game = db.define('game', {
    chat_id: Sequelize.INTEGER,
    date: Sequelize.DATE,
    userId: Sequelize.INTEGER,
})

Game.belongsTo(User, {foreignKey: 'userId'});

const createChat = (chat_id) =>
    Chat.create({
        chat_id: chat_id,
    })


const createUser = (username, chat_id, user_id = -1) =>
    User.create({
        chat_id: chat_id,
        username: username,
        user_id: user_id,
        is_active: true,
        count: 0,
    })

const createGame = (chat_id, user_id) =>
    Game.create({
        chat_id: chat_id,
        date: new Date(),
        userId: user_id,
    })

const updateGame = (chat_id, last_game_id) =>
    Chat.update({
        last_game_id: last_game_id,
    }, {
            where: {
                id: chat_id
            }
        });

const getChatId = (chat_id) =>
    Chat.findOne({ where: { chat_id: chat_id } })


const getUserById = (id) =>
    User.findOne({ where: { id: id } })

const getUserByUserId = (id) =>
    User.findOne({ where: { user_id: id } })

const getUserByName = (username) =>
    User.findOne({ where: { username: username } })

const getUsers = (chat_id) =>
    User.findAll({ where: { chat_id: chat_id } })

const getActiveUsers = (chat_id) =>
    User.findAll({ where: { chat_id: chat_id, is_active: true } }).then(result => result || [])

const getGames = (chat_id) =>
    Game.count({ 
        attributes: ['userId'],
        include: [User],
        where: { chat_id: chat_id },
        group: ['userId'],
        order: [['count'], 'DESC']
    })

const getLastGame = (chat_id, last_game_id) =>
    Game.findOne({ where: { chat_id: chat_id, id: last_game_id } })


db.sync();
// db.sync({force:true});

module.exports = {
    getChatId,
    getUserById,
    createUser,
    createChat,
    getActiveUsers,
    createGame,
    updateGame,
    getLastGame,
    getUserByName,
    getUserByUserId,
    getGames,
}