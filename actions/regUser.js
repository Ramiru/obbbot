const db = require('../db');

const createUsersFromArray = (array, chat_id, ctx) => {
    array.forEach((item, index) => {
        if (item.startsWith('@')) {
            db.getUserByName(item).then(user => {
                if (!user) {
                    db.createUser(item, chat_id);
                }
            })
        } else {
            let entity = ctx.message.entities[index + 1];
            if (entity && entity.user) {
                db.getUserByUserId(entity.user.id).then(user => {
                    if (!user) {
                        db.createUser(entity.user.first_name, chat_id, entity.user.id);
                    }
                })
            }
        }
    })
}

const getChat = (ctx) =>
    db.getChatId(ctx.chat.id).then(chat => (
        chat || db.createChat(ctx.chat.id).then(newChat => {
            return newChat
        })
    ));

const regUsers = (ctx) => {
    let array = ctx.message.text.split(' ').slice(1).filter(item => (item));
    getChat(ctx).then(chat => {
        createUsersFromArray(array, chat.id, ctx);
    })
    ctx.reply(array.join(', ') + ' are registered');
}

module.exports = regUsers;