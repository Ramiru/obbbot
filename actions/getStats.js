const db = require('../db');

const LABELS = ['раз', 'раза', 'раз'];

const nums = (number) => {
    return format(number, LABELS);
}

const format = (number, titles) => {
    const cases = [2, 0, 1, 1, 1, 2];
    return ` ${number} ${titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]]}`;
}


const getChat = (ctx) =>
    db.getChatId(ctx.chat.id).then(chat => (
        chat || db.createChat(ctx.chat.id).then(newChat => {
            return newChat
        })
    ));

const getStats = (ctx) => {
    let userStats = [];
    getChat(ctx).then(chat => {
        db.getGames(chat.id).then(games => {
            ctx.reply('Статистика за все время:');
            games.forEach((element, index) => {
                userStats.push(
                    db.getUserById(element.userId).then(user => {
                        return `${index === 0 ? 'Топ пидор' : ''} ${user.username} - ${nums(element.count)}`;
                    })
                )
            });
            Promise.all(userStats).then(stats => {
                stats.forEach(stat => {
                    ctx.reply(stat);
                })
            })
        })
    })
}

module.exports = getStats;