const db = require('../db');

const createNewGame = (chat_id, users, ctx) => {
    let count = users.length;
    let rand = users[Math.floor(Math.random() * count)];
    return db.createGame(chat_id, rand.id).then(game => {
        db.updateGame(chat_id, game.id).then(() => {
            if (rand.username) {
                ctx.reply(`Пидор дня ${rand.username}`);
            } else {
                ctx.replyWithMarkdown(`Пидор дня [${rand.username}](tg://user?id=${rand.user_id})`);
            }
        })
    });
}

const runGame = (ctx) => {
    db.getChatId(ctx.chat.id).then(chat => {
        if (!chat) {
            return db.createChat(ctx.chat.id).then(newChat => {
                ctx.reply('0 users is ready to play. Please register more players');
            })
        }
        db.getActiveUsers(chat.id).then(users => {
            if (!users.length) {
                return ctx.reply('0 users is ready to play. Please register more players');
            }
            let today = new Date();
            db.getLastGame(chat.id, chat.last_game_id).then(game => {
                if (!game) {
                    return createNewGame(chat.id, users, ctx);
                }
                if (today.toDateString() === game.date.toDateString()) {
                    return db.getUserById(game.userId).then(user => {
                        ctx.reply(`Я уже выбрал пидора дня и это ${user.username}`);
                    });
                } 
                return createNewGame(chat.id, users, ctx);
            })
        })
    })
}

const startGame = (ctx) => {
    let lastPromise;
    if(lastPromise) {
        lastPromise.then(() => runGame(ctx));
    } else {
        lastPromise = runGame(ctx);
    }
}

module.exports = startGame;