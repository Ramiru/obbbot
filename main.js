const { setTimeout } = require('timers');
const {
    getChatId,
    createUser,
    createChat,
    getActiveUsers,
    createGame,
    updateGame,
    getLastGame,
    getUserById,
    getUserByName,
    getUserByUserId,
} = require('./db');

const commands = require('./commands');
const hears = require('./hears');

const Telegraf = require('telegraf')

const bot = new Telegraf(process.env.TG_TOKEN)

bot.telegram.getMe().then((botInfo) => {
    bot.options.username = botInfo.username
})

bot.start((ctx) => {
    console.log('started:', ctx.from.id)
    return ctx.reply('Bot started')
})

commands.forEach(item => {
    bot.command(item.name, item.func);
    bot.command(item.name + '@' + bot.options.username, item.func);
})

bot.hears(/(\S+) не пидор/ig, (ctx) => {
    getChatId(ctx.chat.id).then(chat => {
        if (chat) {
            getLastGame(chat.id, chat.last_game_id).then(game => {
                getUserById(game.user_id).then(user => {
                    if (('@' + ctx.message.from.username) === user.username) {
                        ctx.reply(`Не-не, ты то, @${ctx.message.from.username}, тот еще пидорок`);
                    } else {
                        ctx.reply(`Хорошо, хорошо, ${ctx.match[1]} сегодня не пидор`);
                    }
                })
            })
        } else {
            ctx.reply(`Да, @${ctx.message.from.username}, ты не пидор (но это не точно)`);
        }

    })
}
)

bot.hears(/ет$/, (ctx) => {
    let answers = ['Пидора ответ', 'Говна пакет', 'Мамки твоей минет'];
    let index = Math.floor(Math.random() * answers.length);
    ctx.reply(answers[index]);
}
)

bot.hears(/хуй/, (ctx) => {
    ctx.reply(`@${ctx.message.from.username}, хуй у тебя в жопе`);
}
)

bot.hears(/член/, (ctx) => {
    ctx.reply(`Член у тебя за щекой, @${ctx.message.from.username}, проверяй`);
}
)

bot.on('text', (ctx) => {
})
bot.startPolling()