const startGame = require('./actions/startGame');
const regUser = require('./actions/regUser');
const getStats = require('./actions/getStats');

let commands = [
    {
        name: 'game',
        func: startGame,
    },
    {
        name: 'reg',
        func: regUser,
    },
    {
        name: 'stats',
        func: getStats,
    },
]


module.exports = commands;